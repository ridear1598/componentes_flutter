import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

//mateapp nos crea toda la estructura

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Material App',
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],  
         supportedLocales: [
          const Locale('en', 'US'), // English, no country code
          const Locale('es', 'ES'), // Spanish, no country code
        ],
        //home: HomePage()
        initialRoute: '/',
       /* routes     :   <String, WidgetBuilder>{
          '/'      :(BuildContext context)=> HomePage(),
          'alert'  :(BuildContext context)=>AlertPage(),
          'avatar' :(BuildContext context)=>AvatarPage(), 
        },
        */

        routes:getAplicationsRutes(),
        onGenerateRoute: (RouteSettings settings){
            print('ruta llamada: ${settings.name}');
            return MaterialPageRoute(
            builder:(BuildContext context)=>AlertPage()
            );
        },
    );
  }
}
