import 'package:flutter/material.dart';
//stalessw ejecutar para crear codigo

class HomePageTemp extends StatelessWidget {

  final opciones  =['uno', 'dos', 'tres', 'cuatro'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Componenete en flutter')),
      body: ListView(
        children: _createItemsCorta()
      ),
    );
  }


 /* List<Widget> _createItems(){

    List<Widget> list= new List<Widget>(); //creamos una lista 

       for (String opt in opciones) {
          final tempWidget=ListTile(
            leading: Icon(Icons.map),
            title: Text(opt)
          );

          list..add(tempWidget)
              ..add(Divider());
          //list.add(tempWidget);
         // list.add(Divider());
       }
     return list; 
  }
*/

  //Segunda manera

  List<Widget>_createItemsCorta(){
      var widgets=opciones.map((item){

          return Column(  //para crear este widget cntr +. seleccionar wrap with column
            children: <Widget>[
              ListTile(  
                title: Text(item +' didier'),
                subtitle:Text('provando'),  
                leading:Icon(Icons.mail_outline),
                trailing: Icon(Icons.keyboard_arrow_down),
                onTap:  (){},
              ),
              Divider()
            ],
          );
      }).toList();


      return widgets;
  }
}
