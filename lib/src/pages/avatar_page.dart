import 'package:flutter/material.dart';


class AvatarPage extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Abatar Page'), 
        actions:<Widget>[
           Container(
              //margin:EdgeInsets.only(right: 10.0),
              padding: EdgeInsets.all(5.0),
             child: CircleAvatar(
                backgroundImage: NetworkImage('https://www.tierragamer.com/wp-content/uploads/2019/09/One-Piece-Monkey-D-Luffy-Sonrisa-450x300.jpg'),
                radius: 20.0,
              ),
           ),
          Container(
            margin:EdgeInsets.only(right: 10.0),
            child: CircleAvatar(
              child: Text('D'),
              backgroundColor: Colors.brown,
            ),
          )
        ]
      ),
      body:Center(
         child:FadeInImage(
           image: NetworkImage('https://www.ninoversace.com/wp-content/uploads/landscape-mountains-sky-4843193.jpg'),
           placeholder: AssetImage('assets/jar-loading.gif'),
           fadeInDuration:Duration(milliseconds: 200),

         )
      )
    );
  }
}