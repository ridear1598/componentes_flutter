import 'package:flutter/material.dart';



class CardPage extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         title: Text('Cards'),
      ),
      body:ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
            _cardTipo1(),
            SizedBox(height: 30.0,),
            _cardTipo2(),
             SizedBox(height: 30.0,),
            _cardTipo2(),
             SizedBox(height: 30.0,),      _cardTipo1(),
        ],)
    );
  }

 Widget _cardTipo1() {
   return Card(
     elevation: 3.0,
     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.phone_android, color: Colors.blue) ,
            title: Text('Soy el titulo de esta tarjeta'),
            subtitle: Text('Richard didier')
          ),
          Row(
            mainAxisAlignment:  MainAxisAlignment.end,
            children: <Widget>[
              TextButton(onPressed: (){}, child: Text('Cancelar')),
              TextButton(onPressed: null, child: Text('Ok')),
          ],)
        ],
      ),
   );

  }

  Widget _cardTipo2() {
   // final card= Card(
  final card= Container(
        child: Column(
          children: <Widget>[
            FadeInImage(
              placeholder: AssetImage('assets/jar-loading.gif'), 
              image: NetworkImage('https://www.ninoversace.com/wp-content/uploads/landscape-mountains-sky-4843193.jpg'),
              fadeInDuration: Duration(milliseconds: 200),
              height: 300.0,
              fit:BoxFit.cover
              ),    
                /*Image(
                  image: NetworkImage('https://www.ninoversace.com/wp-content/uploads/landscape-mountains-sky-4843193.jpg'),
                ),
                */
                Container(
                  padding: EdgeInsets.all(8.0),
                  child: Text('didier')
                  )
          ],
        ),
    );
    return Container(
      decoration: BoxDecoration(
         borderRadius:BorderRadius.circular(30.0),
         color:Colors.white,
         boxShadow:  <BoxShadow>[
            BoxShadow(
              color:Colors.black26,
              blurRadius: 10.0,
              spreadRadius: 2.0,
              offset:Offset(2.0,10.0)
            )
         ] 
      ),
      child: ClipRRect(
         borderRadius:BorderRadius.circular(30.0),
         child: card,
      ),
    );
  }

}
