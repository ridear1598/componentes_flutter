import 'package:flutter/material.dart';


class AlertPage extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alert Page')
      ),
      body: Center(
        child: ElevatedButton(
          child:Text('Mostrar alerta'),
          style: ElevatedButton.styleFrom(
             primary: Colors.red,
             shape:StadiumBorder(),
          ), onPressed: ()=>_mostrarlerta(context),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () { 
          Navigator.pop(context);
         },
        child: Icon(Icons.add_location),
      ),
    );
  }
  void _mostrarlerta(BuildContext context){
     showDialog(
       context: context,
       barrierDismissible: true,
       builder: (contex){
          return AlertDialog(
              shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
              title: Text('didier'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                   Text('Este es el contenido de la caja'),
                   FlutterLogo(size: 100.0),           
                ],
              ),
              actions  : <Widget>[
                       TextButton(
                         onPressed:()=>Navigator.of(context).pop(),
                        child: Text('Aceptar'),),
                       TextButton(onPressed: () {  
                         Navigator.of(context).pop();
                       }, child: Text('Cancelar'),)
              ]
          );
       }
       );
  }
}