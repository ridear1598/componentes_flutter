
import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget {
  
  @override
  _AnimatedContainerPageState createState() => _AnimatedContainerPageState();
}

class _AnimatedContainerPageState extends State<AnimatedContainerPage> {
    double _width=50.0;
    double _height=50.0;
    Color _color=Colors.pink;  

    BorderRadiusGeometry _borderRadius=BorderRadius.circular(8.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
          title: Text('Animated container')
       ),
       body:Center(
         child: AnimatedContainer(
           duration:Duration(seconds: 1),
           curve: Curves.fastLinearToSlowEaseIn,
           width: _width,
           height: _height,
           decoration: BoxDecoration(
             borderRadius: _borderRadius,
             color: _color
           ),
         ),       
       ),
       floatingActionButton: FloatingActionButton(
         onPressed: _cambiarEstado,
        child:Icon(Icons.play_arrow)
       ) ,
       //child: child,
    );  
  }

  void _cambiarEstado() {
     final randomNumero = Random();
    setState(() {
     
      _width=randomNumero.nextInt(300) + 10.toDouble();
      _height=randomNumero.nextInt(300) + 10.toDouble();
      _color=Color.fromARGB(
        randomNumero.nextInt(255), 
        randomNumero.nextInt(255), 
        randomNumero.nextInt(255), 
        randomNumero.nextInt(255));
      _borderRadius=BorderRadius.circular(randomNumero.nextInt(100).toDouble());
    });
  }
}