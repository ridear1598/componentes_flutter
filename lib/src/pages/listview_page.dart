import 'dart:async';

import 'package:flutter/material.dart';



class ListViewPage extends StatefulWidget {
  
  @override
  _ListViewPageState createState() => _ListViewPageState();
}

class _ListViewPageState extends State<ListViewPage> {
    ScrollController _scrollController= new ScrollController();
    List<int> _listaNumeros=[];
    int _ultimoNumero=0;
    bool _isLoading=false;

    @override
  void initState() {
    super.initState();
    _agregar10();

    _scrollController.addListener(() {
        //print('scrooll');
        if (_scrollController.position.pixels==_scrollController.position.maxScrollExtent){
           // _agregar10();
           fetchData();
        }
    });
  }


  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Listview'),
          backgroundColor: Colors.blueAccent,
          actions: <Widget>[
            IconButton(
                icon: const Icon(Icons.add_alert),
                tooltip: 'Show Snackbar',
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('This is a snackbar'))
                  );
                },
              ),
          ],
        ),
        body:Stack(
          children: <Widget>[
               _crearLista(),
               _crearLoding(),
          ],)
       
        
       //child: child,
    );
  }

  Widget _crearLista() {

    return  RefreshIndicator(
        onRefresh:obtenerPagina1,
        child: ListView.builder(
        controller:_scrollController,
        itemCount: _listaNumeros.length,
        itemBuilder: (BuildContext context, int index){
            final imagen=_listaNumeros[index];
          return FadeInImage(
              placeholder: AssetImage('assets/jar-loading.gif'),
             image: NetworkImage('https://picsum.photos/450/300?image=$imagen'));
        }
        ),
    );
  }

  Future<Null>obtenerPagina1() async{
      final duration = new Duration(seconds: 2);
      new Timer(
        duration, (){
          _listaNumeros.clear();
          _ultimoNumero++;
          _agregar10();
        });
        return Future.delayed(duration);
  }

 void  _agregar10(){
     for (var i = 0; i < 10; i++) {
       _ultimoNumero++;
       _listaNumeros.add(_ultimoNumero);
     }
     setState(() {});
  }

  Future<Null> fetchData() async{
   _isLoading =true;
   setState(() {});

    final duration= new Duration(seconds: 2);
    new Timer(duration, respuestaHttp);

    return Future.delayed(duration);
  }

  void respuestaHttp(){
      _isLoading=false;

      _scrollController.animateTo(
        _scrollController.position.pixels+100,
        curve:Curves.fastOutSlowIn,
        duration:Duration(milliseconds: 250)
      );

      _agregar10();


  }

  Widget _crearLoding() {
        if (_isLoading){
          return Column(
            mainAxisSize:MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children:<Widget> [
              Row(
                mainAxisAlignment:MainAxisAlignment.center,
                children:<Widget> [
                  CircularProgressIndicator(),
                ],
              ),
              SizedBox(height: 30.0)
            ],
          );
        }else{
          return Container();
        }

  }
}



