import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double _valorSlider=100.0;
  bool _valorCheck=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text('Sliders')
       ),
       body: Container(
         padding: EdgeInsets.only(top:50.0),
          child: Column(
            children:<Widget> [
                _crearSlider(),
                Divider(),
                Text('El valor es: $_valorSlider'),
                Divider(),
                _crearCheckbox(),
                Divider(),
                _crearSwitch(),
                Divider(),
                Expanded(child: _crearImgen()),
          ],
          ),
       ),
    );
  }

  Widget _crearSlider() {
    return Slider(
      activeColor: Colors.indigoAccent,
      label: 'Tamaño de la imagen',
     // divisions: 20,
      value:_valorSlider,
      min:10.0,
      max:400.0,
      onChanged: (_valorCheck)?null: (value) {
        setState(() {
        _valorSlider=value;
        });
          
       },

    );
  }

  Widget _crearCheckbox() {
    //  return Checkbox(
    //    onChanged: (bool value) { 
    //      setState(() {
    //       _valorCheck=value;
    //      }); 
        
    //    }, 
    //    value: _valorCheck,

    //  ); 

    return CheckboxListTile(
      title: Text('Bloquear slider'),
      value: _valorCheck, 
      onChanged: (bool value) { 
         setState(() {
          _valorCheck=value;
         });   
       },
      );
  }

  Widget _crearImgen() {
      return Image(
        image: NetworkImage('https://www.ninoversace.com/wp-content/uploads/landscape-mountains-sky-4843193.jpg'),
        width: _valorSlider,
        fit: BoxFit.contain,
      );
  }

  Widget _crearSwitch() {
    return SwitchListTile(
      title: Text('Bloquear slider'),
      value: _valorCheck, 
      onChanged: (bool value) { 
         setState(() {
          _valorCheck=value;
         });   
       },
      );

  }

}