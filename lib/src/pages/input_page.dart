import 'package:flutter/material.dart';



class InputPage extends StatefulWidget {


  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {

  String _nombre='';
  String _email='';
  String _pass='';
  String _fecha='';
  String _poderSeleccionado='Volar';
  List<String> __poderes=['Volar', 'nadar', 'fuerza','velocidad'];

  TextEditingController _inputFieldDateController=new TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Center(
           child: Text('Inputs'))
       ),
       body:ListView(
         padding: EdgeInsets.symmetric(vertical: 10.0, horizontal:20.0),
         children: <Widget>[
           _crearInput(),
           Divider(),
           _crearEmail(),
           Divider(),
           _crearPassword(),
             Divider(),
          _crearFecha(context),
           Divider(),
           _crearDrwodown(),
          Divider(),
           _crearPersona(),
         ]
      ),
    );
  }
    Widget _crearInput() {
      return TextField(
        //autofocus: true,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            ),
          counter: Text('Letras ${_nombre.length}'),
          hintText: 'Nombre de la persona',
          labelText: 'Nombre',
          helperText: 'Solo es el nombre',
          suffixIcon: Icon(Icons.accessibility),
          icon:Icon(Icons.account_circle)
        ),
        onChanged: (valor){
          setState(() {
               _nombre = valor;
          //print(_nombre);
          });
       
        },

      );

  }


 Widget _crearEmail() {
   return TextField(
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            ),
          hintText: 'Email',
          labelText: 'Email',
          suffixIcon: Icon(Icons.alternate_email),
          icon:Icon(Icons.email)
        ),
        onChanged: (valor)=>
          setState(() {
           _email=valor;
          })
       
        ,

      );
  }


  Widget _crearPassword() {
   return TextField(
          obscureText: true,
          decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            ),
          hintText: 'Contraseña',
          labelText: 'Contraseña',
          suffixIcon: Icon(Icons.lock),
          icon:Icon(Icons.lock_open)
        ),
        onChanged: (valor)=>
          setState(() {
           _pass=valor;
          })
      );
  }

  Widget _crearFecha(BuildContext context) {
     return TextField(
          controller: _inputFieldDateController,
          enableInteractiveSelection: true,
          decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            ),
          hintText: 'Fecha de nacimiento',
          labelText: 'Fecha de nacimiento',
          suffixIcon: Icon(Icons.calendar_today),
          icon:Icon(Icons.calendar_today_sharp)
        ),
       onTap: (){

         FocusScope.of(context).requestFocus(new FocusNode());
         _selectDate(context);
       },
      );
  }

   _selectDate(BuildContext context) async  {
    DateTime picked=  await showDatePicker(
      context: context, 
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2018), 
      lastDate: new DateTime(2025),
      locale: Locale('es','ES')
      );

      if  (picked!=null){
          setState(() {
              _fecha=picked.toString();
              _inputFieldDateController.text=_fecha;
          });
      }
  }
  List<DropdownMenuItem<String>> getOpcionesDropdown(){
     List<DropdownMenuItem<String>> lista= []; 
    __poderes.forEach((poder)=>{
        lista.add(DropdownMenuItem(
          child: Text(poder),
          value:poder
        ))
      });
    return lista;
  }

  Widget _crearDrwodown() {
    return Row(
     children: <Widget>[
      Icon(Icons.select_all),
      SizedBox(width: 30.0),

      Expanded(
        child: DropdownButton(
          value:_poderSeleccionado,
          items: getOpcionesDropdown(),
          onChanged: (opt){
              setState(() {
              _poderSeleccionado=opt;       
              });
            },
        ),
      )
      ],
    );
    
    

  }

  Widget _crearPersona() {
   final column=Column(
        children: <Widget>[
          ListTile(
            title:Text('Nombre es :$_nombre'),
          ),
           ListTile(
            title:Text('El email  es :$_email'),
          ),
          ListTile(
            title:Text('El email  es :$_pass'),
          ),
          ListTile(
              title:Text('El poder es :$_poderSeleccionado')
          )
        ],
   );
    return  column;
  

 }
}